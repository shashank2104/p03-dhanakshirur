//
//  GameScreen.m
//  p03-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 2/14/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import "GameScreen.h"

@implementation GameScreen
@synthesize paddle, ball, bricks, gameOver, gameWon, gameScreen;
@synthesize timer;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
int x = 0, y=80, z = 20, len = 40;

-(void)createPlayField
{
    
    CGRect bounds = [self bounds];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
  //  NSLog(@"%f", bounds.size.width);
    self.gameWon.hidden = YES;
    self.gameOver.hidden = YES;
    [self addSubview:gameWon];
    [self addSubview:gameOver];
     paddle = [[UIView alloc] initWithFrame:CGRectMake(100, 480, 60, 10)];
     [self addSubview:paddle];
     [paddle setBackgroundColor:[UIColor yellowColor]];
     
     ball = [[UIView alloc] initWithFrame:CGRectMake(130, 470, 10, 10)];
     [self addSubview:ball];
     [ball setBackgroundColor:[UIColor redColor]];
     
     self.bricks = [[NSMutableArray alloc] init];
     
     
     for(int i=0; i<4; i++) {
         while(islessequal( (x+len), bounds.size.width)) {
             
             CGFloat hue = ( arc4random() % 256 / 256.0 );
             CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;
             CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;
             
             UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(x, y, len , z)];
             [self addSubview:brick];
             
             [brick setBackgroundColor:[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]];
             [self.bricks addObject:brick];
             x+=40;
         }
         CGFloat hue1 = ( arc4random() % 256 / 256.0 );
         CGFloat saturation1 = ( arc4random() % 128 / 256.0 ) + 0.5;
         CGFloat brightness1 = ( arc4random() % 128 / 256.0 ) + 0.5;

             len = bounds.size.width - (x+len-40);
             UIView *brick = [[UIView alloc] initWithFrame:CGRectMake(x, y, len , z)];
             [self addSubview:brick];
         [brick setBackgroundColor:[UIColor colorWithHue:hue1 saturation:saturation1 brightness:brightness1 alpha:1]];
         
             [self.bricks addObject:brick];
             len = 40;
             x = 0;
             y += 20;
     //    }


     }
     dx = 10;
     dy = 10;
     
     }

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGRect bounds = [self bounds];
 //   NSLog(@"%f", bounds.size.width);

    
    for (UITouch *t in touches)
    {
        CGPoint p = [t locationInView:self];
        p.y = 485;
   //     NSLog(@"%f", p.x);
        CGPoint p1 = [paddle center];
        if((p1.x + 30) == bounds.size.width)
        {
            break;
        }
        [paddle setCenter:p];
    }
    

}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesBegan:touches withEvent:event];

}

-(IBAction)startAnimation:(id)sender
{
    if (self.gameWon.hidden == NO) {
        self.gameWon.hidden = YES;
        [gameScreen createPlayField];
    }
    timer = [NSTimer scheduledTimerWithTimeInterval:.05	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
}

-(IBAction)stopAnimation:(id)sender
{
    [timer invalidate];
}

-(void)timerEvent:(id)sender
{
    CGRect bounds = [self bounds];
    
    CGPoint p = [ball center];
    
    if ((p.x + dx) < 0)
        dx = -dx;
    
    if ((p.y + dy) < 0)
        dy = -dy;
    if ((p.x + dx) > bounds.size.width)
        dx = -dx;
    
    if ((p.y + dy) > bounds.size.height) {
        dy = -dy;
        for(int i=0; i<bricks.count; i++)
            [[bricks objectAtIndex:i] setHidden:YES];
        self.gameOver.hidden = NO;
        [paddle setHidden:YES];
        [ball setHidden:YES];

    }
    
    p.x += dx;
    p.y += dy;
    [ball setCenter:p];
    
    // Now check to see if we intersect with paddle.  If the movement
    // has placed the ball inside the paddle, we reverse that motion
    // in the Y direction.
    if (CGRectIntersectsRect([ball frame], [paddle frame]))
    {
        dy = -dy;
        p.y += 2*dy;
        [ball setCenter:p];
    }
    for(int i=0; i<bricks.count; i++) {
        if (CGRectIntersectsRect([ball frame], [[self.bricks objectAtIndex:i] frame])) {
            [[self.bricks objectAtIndex:i] setHidden:YES];
            [self.bricks removeObjectAtIndex:i];
            if(bricks.count == 0)
            {
                self.gameWon.hidden = NO;
                [paddle setHidden:YES];
                [ball setHidden:YES];
                
            }
            dy = -dy;
        }
    }
    
    
}

@end
