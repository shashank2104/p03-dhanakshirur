//
//  main.m
//  p03-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 2/14/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
