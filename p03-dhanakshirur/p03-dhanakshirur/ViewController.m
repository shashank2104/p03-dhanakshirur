//
//  ViewController.m
//  p03-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 2/14/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize gameScreen, startButton, stopButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    startButton.layer.cornerRadius = 20;
    stopButton.layer.cornerRadius = 20;

    [gameScreen createPlayField];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
