//
//  AppDelegate.h
//  p03-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 2/14/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

