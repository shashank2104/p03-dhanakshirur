//
//  GameScreen.h
//  p03-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 2/14/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
    float dx, dy;  // Ball motion
}
@property (nonatomic, strong) IBOutlet GameScreen *gameScreen;
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) IBOutlet UIView *gameWon;
@property (nonatomic, strong) IBOutlet UIView *gameOver;


-(void)createPlayField;

@end
