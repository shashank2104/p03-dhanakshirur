//
//  ViewController.h
//  p03-dhanakshirur
//
//  Created by Shashankjayate Dhanakshirur on 2/14/16.
//  Copyright © 2016 sdhanak1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameScreen.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet GameScreen *gameScreen;
@property (nonatomic, strong) IBOutlet UIButton *startButton;
@property (nonatomic, strong) IBOutlet UIButton *stopButton;

@end

